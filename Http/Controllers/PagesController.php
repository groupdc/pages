<?php

namespace Dcms\Pages\Http\Controllers;

use Dcms\Pages\Models\Pageslanguage;
use Dcms\Pages\Models\Pages;
use Dcms\Core\Models\Languages\Language;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use DateTime;
use Config;

class PagesController extends Controller
{
    /**
     * Set permissions.
     *
     */
    public function __construct()
    {
        $this->middleware('permission:pages-browse')->only('index');
        $this->middleware('permission:pages-add')->only(['create', 'store']);
        $this->middleware('permission:pages-edit')->only(['edit', 'update']);
        $this->middleware('permission:pages-delete')->only('destroy');
    }

    public static function QueryTree($language_id = null)
    {
        $tree = DB::connection('project')
            ->table('pages_language as node')
            ->select(
                (DB::connection("project")->raw("CONCAT( REPEAT( '-', node.depth ), node.title) AS page")),
                "node.id",
                "node.parent_id",
                "node.language_id",
                "node.depth",
                (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' >") as regio'))
            )
            ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
            ->where('node.language_id', $language_id)
            ->orderBy('node.lft')
            ->get();

        return $tree;
    }

    public static function CategoryDropdown($models = null, $selected_id = null, $enableNull = true, $name = "parent_id", $key = "id", $value = "page")
    {
        $dropdown = "empty set";
        if (!is_null($models) && count($models) > 0) {
            $dropdown = '<select name="' . $name . '" class="form-control" id="'.$name.'">' . "\r\n";

            if ($enableNull == true) {
                $dropdown .= '<option value="">None</option>'; //epty value will result in NULL database value;
            }

            foreach ($models as $model) {
                $selected = "";
                if (!is_null($selected_id) && $selected_id == $model->$key) {
                    $selected = "selected";
                }

                $dropdown .= '<option ' . $selected . ' value="' . $model->$key . '" class="' . $name . ' language_id' . $model->language_id . ' parent-' . (is_null($model->parent_id) ? 0 : $model->parent_id) . ' depth-' . $model->depth . '">' . $model->$value . '</option>' . "\r\n";
            }
            $dropdown .= '</select>' . "\r\n" . "\r\n";
        }

        return $dropdown;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view
        return View::make('dcms::pages/index');
    }

    public function getDatatable()
    {
        $query =
        DB::connection('project')
        ->table('pages_language as node')
        ->select(
            (DB::connection("project")->raw("CONCAT( REPEAT( '-', node.depth ), node.title) AS page")),
            "node.id",
            "node.page_id",
            (DB::connection("project")->raw('CONCAT("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as regio'))
        )
        ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
        ->orderBy('node.lft');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('language_id', intval(session('overrule_default_by_language_id')));
        }

        return DataTables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/pages/'.$model->id.'" accept-charset="UTF-8" class="pull-right"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">';
                if (Auth::user()->can('pages-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/pages/'.$model->page_id.'/edit"><i class="far fa-pencil"></i></a>';
                }
                if (Auth::user()->can('pages-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';
                return $edit;
            })
            ->rawColumns(['regio', 'edit'])
            ->filterColumn('page', function ($query, $keyword) {
                $query->whereRaw('title like ?', ["%{$keyword}%"]);
            })
            ->make(true);
    }
    
    public function getDatatableWithCheckbox($currentpage = 0, $related_id = 0)
    {
        $query =
        DB::connection('project')
        ->table('pages_language as node')
        ->select(
            (DB::connection("project")->raw("
            case when related_id = '".$related_id."' then 1 else 0 end as checked,
            CONCAT( REPEAT( '-', node.depth ), node.title) AS page")),
            "node.id",
            (DB::connection("project")->raw('CONCAT("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as regio'))
        )
        ->where('node.id', '<>', $currentpage)
        ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
        ->orderBy('checked', 'DESC')
        ->orderBy('node.lft');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('language_id', intval(session('overrule_default_by_language_id')));
        }

        return DataTables::queryBuilder($query)
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="relation_ids[]" value="'.$model->id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->id.'" > ';
            })
            ->rawColumns([ 'radio', 'regio'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $objlanguages = $this->getInformation();
        
        $pageOptionValues = null;
        foreach ($objlanguages as $l) {
            $pageOptionValues[$l->language_id] = $this->CategoryDropdown($this->QueryTree($l->language_id), $l->parent_id, true, 'parent_id['.$l->language_id.']');
        }

        return View::make('dcms::pages/form')
            ->with('page', null)
            ->with('languages', $objlanguages)
            ->with('pageOptionValues', $pageOptionValues);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $Page_isset = false;
        foreach ($request->get("title") as $lang_id => $title) {
            if (strlen(trim($request->get("title")[$lang_id])) > 0) {
                $theParent = null;
                if (intval($request->get("parent_id")[$lang_id]) > 0) {
                    $theParent = Pageslanguage::find($request->get("parent_id")[$lang_id]);
                }

                if ($Page_isset == false) {
                    $Pagejoiner = new Pages();
                    $Pagejoiner->save();
                    $page_id = $Pagejoiner->id;
                    $Page_isset = true;
                }

                $Page = new Pageslanguage;
                $Page->language_id = $lang_id;
                $Page->page_id = intval($page_id);
                $Page->title = $request->get("title")[$lang_id];
                $Page->title_tag = $request->get("title_tag")[$lang_id];
                $Page->description = $request->get("description")[$lang_id];
                $Page->meta_description = $request->get("meta_description")[$lang_id];
                $Page->body = $request->get("body")[$lang_id];
                $Page->thumbnail = $request->get("thumbnail")[$lang_id];
                $Page->properties = $request->get("properties")[$lang_id];
                $Page->template = $request->get("template")[$lang_id];
                $Page->url_slug = Str::slug($request->get("title")[$lang_id]);
                $Page->admin = Auth::guard('dcms')->user()->username;
                $Page->save();

                if (is_null($theParent)) {
                    $Page->saveAsRoot();
                } else {
                    $Page->appendToNode($theParent)->save();
                }

                $sort = intval($request->get('sort')[$lang_id]);
                $getsibling = null;
                if ($sort > 0 && !is_null($theParent)) {
                    if ($sort == 1) {
                        $Page->prependToNode(Pageslanguage::find($request->get("parent_id")[$lang_id]))->save();
                    } else {
                        //get the sibling on this position
                        $getSibling = Pageslanguage::where('parent_id', $request->get('parent_id')[$lang_id])->orderBy('lft')->skip($sort-1)->take(1)->first();
                    
                        if (!is_null($getSibling)) {
                            // $Page->insertBeforeNode($getSibling);
                            $Page->insertAfterNode($getSibling);
                        } else {
                            $Page->appendToNode(Pageslanguage::find($request->get("parent_id")[$lang_id]))->save();
                        }
                    }
                }
                $Page->saveDepth();
            }
        }
        // redirect
        Session::flash('message', 'Successfully created page!');
        $this->setCanonicalUrl();
        return Redirect::to('admin/pages');
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as title, NULL as parent_id, '' as title_tag, '' as thumbnail,'' as meta_description, '' as description, '' as body, '' as template, '' as properties, NULL as page_id, 0 as id ")), "id as language_id", "language", "country", "language_name")->get();
        } else {
            return DB::connection("project")->select('
                                                    SELECT language_id, languages.language, languages.country, languages.language_name, page_id,
                                                    title, title_tag, thumbnail, meta_description, description, body, template, properties, parent_id, pages_language.id

													FROM pages_language
													LEFT JOIN languages on languages.id = pages_language.language_id
													LEFT JOIN pages on pages.id = pages_language.page_id
													WHERE languages.id is NOT NULL AND page_id = ?

                                                    UNION

                                                    SELECT languages.id , language, country, language_name, NULL, \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', NULL, 0 
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM pages_language WHERE page_id = ?) ORDER BY 1
													', [$id, $id]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($page_id)
    {
        $objlanguages = $this->getInformation($page_id);
        $page = collect($objlanguages)->whereNotNull('page_id')->first();
        
        if (is_null($page)) {
            \App::abort(500);
        }

        $pageOptionValues = null;
        foreach ($objlanguages as $l) {
            $pageOptionValues[$l->language_id] = $this->CategoryDropdown($this->QueryTree($l->language_id), $l->parent_id, true, 'parent_id['.$l->language_id.']');
        }

        // show the edit form and pass the nerd
        return View::make('dcms::pages/form')
            ->with('page', $page)
            ->with('languages', $objlanguages)
            ->with('pageOptionValues', $pageOptionValues);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        Pageslanguage::where('page_id', intval($id))->update(['page_id'=>null]);

        foreach ($request->get("title") as $lang_id => $title) {
            if (strlen(trim($request->get("title")[$lang_id])) > 0) {
                $Page = Pageslanguage::find($request->get("id")[$lang_id]);
                if (is_null($Page)) {
                    $Page = new Pageslanguage();
                }
                $Page->language_id = $lang_id;
                $Page->page_id = intval($id);
                $Page->title = $request->get("title")[$lang_id];
                $Page->title_tag = $request->get("title_tag")[$lang_id];
                $Page->description = $request->get("description")[$lang_id];
                $Page->meta_description = $request->get("meta_description")[$lang_id];
                $Page->body = $request->get("body")[$lang_id];
                $Page->thumbnail = $request->get("thumbnail")[$lang_id];
                $Page->properties = $request->get("properties")[$lang_id];
                $Page->template = $request->get("template")[$lang_id];
                $Page->url_slug = Str::slug($request->get("title")[$lang_id]);
                $Page->admin = Auth::guard('dcms')->user()->username;
                $Page->save();
    
                $setRoot = false;
                $theParent = null;
                $moveParent = true;
    
                if (intval($request->get('parent_id')[$lang_id]) > 0 && $request->get('parent_id')[$lang_id] <> $Page->parent_id) {
                    $moveParent = true;
                    $theParent = Pageslanguage::find($request->get("parent_id")[$lang_id]);
                } elseif (intval($request->get('parent_id')[$lang_id]) <= 0 && $request->get('parent_id')[$lang_id] <> $Page->parent_id) {
                    $moveParent = true;
                    if (is_null($theParent)) {
                        $setRoot = true;
                    }
                } else {
                    $moveParent = false;
                }
    
                if ($setRoot == true) {
                    $Page->saveAsRoot();
                } elseif (!is_null($theParent)) {
                    $Page->appendToNode($theParent)->save();
                }

                $sort = intval($request->get('sort')[$lang_id]);
                $getsibling = null;
                if ($sort > 0) {
                    if ($sort == 1) {
                        $Page->prependToNode(Pageslanguage::find($request->get("parent_id")[$lang_id]))->save();
                    } else {
                        //get the sibling on this position
                        $getSibling = Pageslanguage::where('parent_id', $request->get('parent_id')[$lang_id])->orderBy('lft')->skip($sort-1)->take(1)->first();
                    
                        if (!is_null($getSibling)) {
                            // $Page->insertBeforeNode($getSibling);
                            $Page->insertAfterNode($getSibling);
                        } else {
                            $Page->appendToNode(Pageslanguage::find($request->get("parent_id")[$lang_id]))->save();
                        }
                    }
                }
                $Page->saveDepth();
            }
        }
    
        Session::flash('message', 'Successfully updated page!');
        $this->setCanonicalUrl();
        return Redirect::to('admin/pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Page = Pageslanguage::find($id);
        $Page->delete();

        Session::flash('message', 'Successfully deleted the page!');
        $this->setCanonicalUrl();
        return Redirect::to('admin/pages');
    }

   
    public function setCanonicalUrl()
    {
        $p = Pageslanguage::orderBy('lft')->get();
        $prefix_depth = [];
        foreach ($p as $q) {
            if ($q->depth == 0) {
                continue;
            }
            $prefix_depth[$q->depth] = $q->url_slug;
            $currentcount = count($prefix_depth);
            for ($x = $q->depth; $x <= $currentcount; $x++) {
                $unset = $x+1;
                unset($prefix_depth[$unset]);
            }
            $canonicalurl = implode('/', $prefix_depth);

            $q->canonical_url = str_replace('index/', '', $canonicalurl);
            $q->save();
        }
        
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://dcm-info.be/nl/', canonical_url) WHERE canonical_url IS NOT NULL AND language_id = 1"));
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://dcm-info.be/fr/', canonical_url)  WHERE canonical_url IS NOT NULL AND language_id = 2"));
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://dcm-info.nl/', canonical_url) WHERE canonical_url IS NOT NULL AND language_id = 3"));
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://dcm-info.fr/', canonical_url) WHERE canonical_url IS NOT NULL AND language_id = 6"));
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://cuxin-dcm.de/', canonical_url) WHERE canonical_url IS NOT NULL AND language_id = 7"));
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://dcm.green/en/', canonical_url) WHERE canonical_url IS NOT NULL AND language_id = 10"));
        DB::SELECT(DB::RAW("UPDATE pages_language SET canonical_url = concat('https://dcm-info.com/int/en/', canonical_url) WHERE canonical_url IS NOT NULL AND language_id = 11"));
    }
}
