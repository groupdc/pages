<?php



Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

            //PAGES
            Route::group(array("prefix" => "pages", "as"=>"pages."), function () {
                Route::any('api/table', array('as'=>'api.table', 'uses' => 'PagesController@getDatatable'));
                Route::any('api/relatedtable/{currentpage?}/{related_id?}', array('as'=>'api.relatedtable', 'uses' => 'PagesController@getDatatableWithCheckbox'));
            });
            Route::resource('pages', 'PagesController');

        });
    });
});
