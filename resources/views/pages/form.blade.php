@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
      <h1>Pages</h1>
      <ol class="breadcrumb">
        <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
        <li><a href="{!! URL::to('admin/pages') !!}"><i class="far fa-file"></i> Pages</a></li>
        @if(!is_null($page))
            <li class="active"><i class="far fa-pencil"></i> Page {{$page->page_id}}</li>
        @else
            <li class="active"><i class="far fa-plus-circle"></i> Create page</li>
        @endif
      </ol>
    </div>

    <div class="main-content">
        @if(!is_null($page))
          {!! Form::model($page, array('route' => array('admin.pages.update', $page->page_id), 'method' => 'PUT')) !!}
        @else
            {!! Form::open(array('url' => 'admin/pages')) !!}
        @endif
        
        @if($errors->any())
          <div class="alert alert-danger">{!! Html::ul($errors->all()) !!}</div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <!-- start new -->
                <div class="main-content-block">
                    @if(isset($languages))
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($languages as $key => $language)
                                <li class="{{ ($key == 0 ? 'active' : '') }}"><a href="{{ '#' . $language->language . '-' . $language->country }}" role="tab" data-toggle="tab"><img src="{{'/packages/Dcms/Core/images/flag-'.strtolower($language->country). '.svg'}}" width="18" height="12" /> {{ $language->language_name }}</a></li>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                            @foreach($languages as $key => $information)
                                <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval(1)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">

                                    <input type="hidden" name="id[{{ $information->language_id }}]" value="{{$information->id}}" />

                                    <div class="form-group">
                                        {!! Form::label('parent_id[' . $information->language_id . ']', 'Parent page') !!}
                                        {!! $pageOptionValues[$information->language_id] !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('sort[' . $information->language_id . ']', 'Sort ') !!}
                                        {!! Form::text('sort[' . $information->language_id . ']', null, array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('title[' . $information->language_id . ']', 'Title ') !!}
                                        {!! Form::text('title[' . $information->language_id . ']', (old('title[' . $information->language_id . ']') ? old('title[' . $information->language_id . ']') : $information->title), array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('title_tag[' . $information->language_id . ']', 'Title tag') !!}
                                        {!! Form::text('title_tag[' . $information->language_id . ']', (old('title_tag[' . $information->language_id . ']') ? old('title_tag[' . $information->language_id . ']') : $information->title_tag), array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('thumbnail', 'Thumbnail') !!}
                                        <div class="input-group">
                                            {!! Form::text('thumbnail[' . $information->language_id . ']', (old('thumbnail[' . $information->language_id . ']') ? old('thumbnail[' . $information->language_id . ']') : $information->thumbnail), array('class' => 'form-control')) !!}
                                            <span class="input-group-btn">
                                                {!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server', 'id'=>'browse_thumbnail'.$information->language_id)) !!}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('meta_description[' . $information->language_id . ']', 'Meta description') !!}
                                        {!! Form::textarea('meta_description[' . $information->language_id . ']', (old('meta_description[' . $information->language_id . ']') ? old('meta_description[' . $information->language_id . ']') : $information->meta_description), array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description[' . $information->language_id . ']', 'Description') !!}
                                        {!! Form::textarea('description[' . $information->language_id . ']', (old('description[' . $information->language_id . ']') ? old('description[' . $information->language_id . ']') : $information->description), array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('body[' . $information->language_id . ']', 'Body') !!}
                                        {!! Form::textarea('body[' . $information->language_id . ']', (old('body[' . $information->language_id . ']') ? old('body[' . $information->language_id . ']') : $information->body), array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('template[' . $information->language_id . ']', 'Template') !!}
                                        {!! Form::text('template[' . $information->language_id . ']', (old('template[' . $information->language_id . ']') ? old('template[' . $information->language_id . ']') : $information->template), array('class' => 'form-control')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('properties[' . $information->language_id . ']', 'Properties ') !!}
                                        {!! Form::text('properties[' . $information->language_id . ']', (old('properties[' . $information->language_id . ']') ? old('properties[' . $information->language_id . ']') : $information->properties), array('class' => 'form-control')) !!}
                                    </div>
                                    
                                </div>
                            @endforeach
                        </div>
                    @endif
                <!-- end new -->
                </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section("script")

<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap-datetimepicker.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('/packages/Dcms/Core/css/bootstrap-datetimepicker.min.css') !!}">

<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKFinder
            $(".browse-server").click(function () {
                BrowseServer('Images:/articles/', 'thumbnail');
            })

            $("body").on("click", ".browse-server-files", function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('Files:/', returnid);
            });

            //CKEditor
            $("textarea[id='description[1]']").ckeditor();
            $("textarea[id='body[1]']").ckeditor();
            
            $("textarea[id='description[2]']").ckeditor();
            $("textarea[id='body[2]']").ckeditor();
            
            $("textarea[id='description[3]']").ckeditor();
            $("textarea[id='body[3]']").ckeditor();
            
            $("textarea[id='description[6]']").ckeditor();
            $("textarea[id='body[6]']").ckeditor();
            
            $("textarea[id='description[7]']").ckeditor();
            $("textarea[id='body[7]']").ckeditor();
            
            $("textarea[id='description[8]']").ckeditor();
            $("textarea[id='body[8]']").ckeditor();
            
            $("textarea[id='description[9]']").ckeditor();
            $("textarea[id='body[9]']").ckeditor();
            
            $("textarea[id='description[10]']").ckeditor();
            $("textarea[id='body[10]']").ckeditor();
            
            $("textarea[id='description[11]']").ckeditor();
            $("textarea[id='body[11]']").ckeditor();

            //HELP SETTING THE SIBLING SORT
            var a = [];
            $(".parent_id").each(function(index){
              before  =  $(this).attr('class').indexOf('parent-');
              after   =  $(this).attr('class').indexOf(' depth-');
              a.push($(this).attr('class').substr(before,(after-before)));
            })
            
            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            })
        });


    </script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript">
       
    </script>
@stop

@section('style')
<style type="text/css">
.d-none {
    display: none;
}
</style>
@stop