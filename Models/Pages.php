<?php

namespace Dcms\Pages\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $connection = 'project';
    protected $table  = "pages";
}
