<?php
return [
    "Pages" => [
        "icon"  => "fa-file",
        "links" => [["route" => "admin/pages/", "label" => "Pages", "permission" => 'pages']],
    ],
];
